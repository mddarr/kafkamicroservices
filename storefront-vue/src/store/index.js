import Vuex from 'vuex';
import Vue from 'vue';
import auth from './modules/auth'
import eccomerce from './modules/ecommerce'

// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
  modules: {
    auth,
    eccomerce,
  }
});
