import Vue from 'vue'
import Router from 'vue-router'
// import GalleryMenu from './components/gallery/GalleryMenu'
import Landing from './components/Landing'

import Register from './components/Register'

import ECommerceIntro from './components/ecommerce/ECommerceIntro'
import Store from './components/ecommerce/Store'
import ProductDetail from './components/ecommerce/ProductDetail'
import ShoppingCart from './components/ecommerce/ShoppingCart'

Vue.use(Router)

export default new Router({
  mode:'history',
  base: process.env.BASE_URL,
  routes: [

    {
      path: '/',
      redirect: '/storefront',
      component: Landing
    },

    {
      path: '/landing',
      component: Landing
    },

    {
      path: '/ecommerce',
      component: ECommerceIntro
    },

    {
      path: '/storefront',
      component: Store
    },

    { 
      path: '/product/:id',
      component: ProductDetail 
    },

    {
      path:'/cart',
      component: ShoppingCart
    },

    {
      path:'/register',
      name:'register',
      component: Register
    },



  ]
})
