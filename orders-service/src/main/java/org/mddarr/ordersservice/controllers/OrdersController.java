package org.mddarr.ordersservice.controllers;

import org.mddarr.ordersservice.dto.Order;
import org.mddarr.ordersservice.dto.OrderRequest;
import org.mddarr.ordersservice.services.OrderService;
import org.mddarr.ordersservice.utils.Utils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;

@RestController
@RequestMapping("orders")
public class OrdersController {
    private final OrderService orderService;

    public OrdersController(OrderService service){
        this.orderService = service;
    }

    @GetMapping
    public List<Order> allOrders() {
        return orderService.getAll();
    }

    @GetMapping("/{order}/{creationDate}")
    public ResponseEntity<Order> getOrder(@PathVariable String order, @PathVariable long creationDate){
        return ResponseEntity.of(orderService.get(order, creationDate));
    }

    @GetMapping("/customerOrders/{customerID}")
    public List<Order> getCustomerOrders(@PathVariable String customerID){
        return orderService.fetchCustomersOrders(customerID);
    }

    @GetMapping("customerOrders/{customerID}/{creationDate}")
    public List<Order> getCustomerOrders(@PathVariable String customerID, @PathVariable String creationDate){
        return orderService.fetchCustomersOrders(customerID, creationDate);
    }


    @DeleteMapping("/{order}/{creationDate}")
    public ResponseEntity<Void> delete(@PathVariable String order,@PathVariable long creationDate){
        orderService.delete(order, creationDate);
        return ResponseEntity.accepted().build();
    }

    @PutMapping
    public ResponseEntity<Order> postOrder(@RequestBody OrderRequest order){
        Order resp = orderService.createOrder(order);
        HttpHeaders headers = new HttpHeaders();
        return ResponseEntity.accepted().headers(headers).body(resp);
    }


}