package org.mddarr.ordersservice.port;

import org.mddarr.orders.event.dto.AvroOrder;


public interface OrderServicePublish {
    void sendOrder(AvroOrder order);
}
