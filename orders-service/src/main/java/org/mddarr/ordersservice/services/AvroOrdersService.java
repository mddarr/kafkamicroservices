package org.mddarr.ordersservice.services;

import org.mddarr.orders.event.dto.AvroOrder;
import org.mddarr.ordersservice.port.OrderServicePublish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AvroOrdersService implements OrderServicePublish {

    @Autowired
    private KafkaTemplate<String, AvroOrder> kafkaTemplateOrder;
    private static final Logger logger = LoggerFactory.getLogger(AvroOrdersService.class);

    @Override
    public void sendOrder(AvroOrder order) {
        logger.info("Send order  {}", order);
        kafkaTemplateOrder.send("orders", order);
    }

}
