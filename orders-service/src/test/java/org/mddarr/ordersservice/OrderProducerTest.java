package org.mddarr.ordersservice;


import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.joda.time.DateTime;
import org.junit.Test;

import org.mddarr.orders.event.dto.AvroOrder;
import org.mddarr.orders.event.dto.OrderState;

import org.mddarr.ordersservice.AbstractKafkaTest;
import org.mddarr.ordersservice.services.AvroOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderProducerTest extends AbstractKafkaTest {



    @Autowired
    private AvroOrdersService orderAvroProducer;


    @Test
    public void should_send_order() {
        orderAvroProducer.sendOrder(new AvroOrder("orderID1","jerry@gmail.com",OrderState.PENDING, "Osprey", 1L, 2.1, new DateTime()));
        ConsumerRecord<String, AvroOrder> singleRecord = KafkaTestUtils.getSingleRecord(orderConsumer, "orders");
        assertThat(singleRecord).isNotNull();
    }


}