package org.mddarr.productsservice.controllers;

import org.mddarr.productsservice.dto.Product;
import org.mddarr.productsservice.services.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("products")
@CrossOrigin
public class ProductsController {

    private final ProductService productService;

    public ProductsController(ProductService productService){
        this.productService = productService;
    }

    @GetMapping
    public List<Product> allProducts() {
        return productService.fetchAllProducts();
    }

    @GetMapping("/{brandID}")
    public List<Product> allProducts(@PathVariable String brandID) {
        return productService.fetchAllProductsByBrand(brandID);
    }

//    @GetMapping
//    public List<Product> allBrandsProducts(){
//
//    }

}
