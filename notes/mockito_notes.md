### Mockito

With JUnit 5 RunWith has been replaced by ExtendWith

@ExtendWith(MockitoExtension.class)


JUnitRunner

The most used widely used annotation in Mockito is @Mock. We can use @Mock to create and inject mocked instances without having to call Mockito.mock manually.

@Mock creates a mock
@InjectMocks creates an instance of the class & injects the mocks that are created with @Mock or @Spy annotations into this instance


Integration Tests

    
