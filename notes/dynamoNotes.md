aws dynamodb query \
    --table-name Orders \
    --key-condition-expression "orderID = :order" \
    --expression-attribute-values  '{":order":{"S":"20171129-29970"}}' \
    --endpoint-url http://localhost:8000

    
aws dynamodb query \
    --table-name Orders \
    --key-condition-expression "customerID = :customer" \
    --expression-attribute-values  '{":customer":{"S":"jerry@gmail.com"}}'