### Kafka Integration Testing
https://docs.spring.io/spring-kafka/reference/html/#embedded-kafka-junit5
The spring-kafka-test jar contains some useful utilities to assist with testing your applications.

KafkaTestUtils provides some static methods to set up producer and consumer properties. The following listing shows those method signatures:

We generally recommend that you use the rule as a @ClassRule to avoid starting and stopping the broker between tests (and use a different topic for each test). Starting with version 2.0, if you use Spring’s test application context caching, you can also declare a EmbeddedKafkaBroker bean, so a single broker can be used across multiple test classes. For convenience, we provide a test class-level annotation called @EmbeddedKafka to register the EmbeddedKafkaBroker bean. The following example shows how to use it:


Starting with version 2.3, there are two ways to use the @EmbeddedKafka annotation with JUnit5. When used with the @SpringJunitConfig annotation, the embedded broker is added to the test application context. You can auto wire the broker into your test, at the class or method level, to get the broker address list.

When not using the spring test context, the EmbdeddedKafkaCondition creates a broker; the condition includes a parameter resolver so you can access the broker in your test method…​

