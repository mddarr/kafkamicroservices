### Spring Notes

Dependency Injection:
    - Dependency: An object requires objects of other classes to perform its operations
    - Injection: Process of providing the required dependencies to an object
    
Constructor Injection

In constructor-based injection, the dependencies required for the class are provided as arguments to the constructor:

@Component
class Cake {

  private Flavor flavor;

  Cake(Flavor flavor) {
    Objects.requireNonNull(flavor);
    this.flavor = flavor;
  }

  Flavor getFlavor() {
    return flavor;
  }
  ...
}

Before Spring 4.3, we had to add an @Autowired annotation to the constructor. With newer versions, this is optional if the class has only one constructor.

In the Cake class above, since we have only one constructor, we don’t have to specify the @Autowired annotation. Consider the below example with two constructors:

When we have a class with multiple constructors, we need to explicitly add the @Autowired annotation to any one of the constructors so that Spring knows which constructor to use to inject the dependencies.

Field Injection
With field-based injection, Spring assigns the required dependencies directly to the fields on annotating with @Autowired annotation.

@Component
class IceCream {

  @Autowired
  private Topping toppings;

  Topping getToppings() {
    return toppings;
  }

  void setToppings(Topping toppings) {
    this.toppings = toppings;
  }

}



In this example, we let Spring inject the Topping dependency via field injection:

Setter Injection
In setter-based injection, we provide the required dependencies as field parameters to the class and the values are set using the setter methods of the properties. We have to annotate the setter method with the @Autowired annotation.



@PostConstruct
The PostConstruct annotation is used on a method that needs to be executed after dependency injection is done to perform any initialization. This method MUST be invoked before the class is put into service. This annotation MUST be supported on all classes that support dependency injection. The method annotated with PostConstruct MUST be invoked even if the class does not request any resources to be injected. Only one method can be annotated with this annotation. The method on which the PostConstruct annotation is applied MUST fulfill all of the following criteria: 
