## Maven dependency scopes

Dependencies with this scope are available on the classpath of the project in all build tasks and they're propagated to the dependent projects.

