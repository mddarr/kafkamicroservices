import boto3
import json

def register_orders_service_task(task_name):
    client = boto3.client('ecs')
    instance_list = client.list_container_instances(cluster='DakobedCluster')['containerInstanceArns']
    response = client.register_task_definition(
        family=task_name,
        networkMode='awsvpc',
        taskRoleArn='arn:aws:iam::710339184759:role/dakobed-ecs-dynamo-role',
        executionRoleArn='arn:aws:iam::710339184759:role/ecsTaskExecutionRole',
        requiresCompatibilities=['FARGATE'],
        cpu='256',
        memory='512',
        containerDefinitions=[
            {
                'name': 'dakobedcontainer',
                'image': '710339184759.dkr.ecr.us-west-2.amazonaws.com/dakobed/services:latest',
                'cpu': 256,
                'memory': 512,
                'memoryReservation': 123,
                'logConfiguration': {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "dakobedservice-logs",
                        "awslogs-region": "us-west-2",
                        "awslogs-stream-prefix": "awslogs-dakobedservice-logs"
                    }
                },
                'portMappings': [
                    {
                        'containerPort': 8080,
                        'hostPort': 8080,
                        'protocol': 'http'
                    },
                ],
                'essential': True,
            },
        ],
    )

def register_products_service_task(task_name):
    client = boto3.client('ecs')
    instance_list = client.list_container_instances(cluster='DakobedCluster')['containerInstanceArns']
    response = client.register_task_definition(
        family=task_name,
        networkMode='awsvpc',
        taskRoleArn='arn:aws:iam::710339184759:role/dakobed-ecs-dynamo-role',
        executionRoleArn='arn:aws:iam::710339184759:role/ecsTaskExecutionRole',
        requiresCompatibilities=['FARGATE'],
        cpu='256',
        memory='512',
        containerDefinitions=[
            {
                'name': 'dakobedcontainer',
                'image': '710339184759.dkr.ecr.us-west-2.amazonaws.com/dakobed/services:latest',
                'cpu': 256,
                'memory': 512,
                'memoryReservation': 123,
                'logConfiguration': {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "dakobedservice-logs",
                        "awslogs-region": "us-west-2",
                        "awslogs-stream-prefix": "awslogs-dakobedservice-logs"
                    }
                },
                'portMappings': [
                    {
                        'containerPort': 8080,
                        'hostPort': 8080,
                        'protocol': 'http'
                    },
                ],
                'essential': True,
            },
        ],
    )

def register_inventory_service_task(task_name):
    client = boto3.client('ecs')
    instance_list = client.list_container_instances(cluster='DakobedCluster')['containerInstanceArns']
    response = client.register_task_definition(
        family=task_name,
        networkMode='awsvpc',
        taskRoleArn='arn:aws:iam::710339184759:role/dakobed-ecs-dynamo-role',
        executionRoleArn='arn:aws:iam::710339184759:role/ecsTaskExecutionRole',
        requiresCompatibilities=['FARGATE'],
        cpu='256',
        memory='512',
        containerDefinitions=[
            {
                'name': 'dakobedcontainer',
                'image': '710339184759.dkr.ecr.us-west-2.amazonaws.com/dakobed/services:latest',
                'cpu': 256,
                'memory': 512,
                'memoryReservation': 123,
                'logConfiguration': {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "dakobedservice-logs",
                        "awslogs-region": "us-west-2",
                        "awslogs-stream-prefix": "awslogs-dakobedservice-logs"
                    }
                },
                'portMappings': [
                    {
                        'containerPort': 8080,
                        'hostPort': 8080,
                        'protocol': 'http'
                    },
                ],
                'essential': True,
            },
        ],
    )

