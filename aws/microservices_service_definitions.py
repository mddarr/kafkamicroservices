import boto3


def create_orders_service(service_name, task_name, subnets, security_group_id, target_group_arn):
    client = boto3.client('ecs')
    response = client.create_service(cluster='DakobedCluster',
                                     serviceName=service_name,
                                     launchType='FARGATE',
                                     taskDefinition=task_name,
                                     desiredCount=1,
                                     networkConfiguration={
                                         "awsvpcConfiguration": {
                                             "assignPublicIp": "ENABLED",
                                             "securityGroups": [security_group_id],
                                             "subnets": [subnets['public1'],subnets['public2'], subnets['private1'], subnets['private2']]
                                         },
                                     },
                                     deploymentConfiguration={
                                         'maximumPercent': 100,
                                         'minimumHealthyPercent': 50},
                                    loadBalancers = [
                                        {"containerName":"dakobedcontainer",
                                         "containerPort":8080,
                                         "targetGroupArn":target_group_arn
                                        }
                                    ]),
    return response


def create_inventory_service(service_name, task_name, subnets, security_group_id, target_group_arn):
    client = boto3.client('ecs')
    response = client.create_service(cluster='DakobedCluster',
                                     serviceName=service_name,
                                     launchType='FARGATE',
                                     taskDefinition=task_name,
                                     desiredCount=1,
                                     networkConfiguration={
                                         "awsvpcConfiguration": {
                                             "assignPublicIp": "ENABLED",
                                             "securityGroups": [security_group_id],
                                             "subnets": [subnets['public1'],subnets['public2'], subnets['private1'], subnets['private2']]
                                         },
                                     },
                                     deploymentConfiguration={
                                         'maximumPercent': 100,
                                         'minimumHealthyPercent': 50},
                                    loadBalancers = [
                                        {"containerName":"dakobedcontainer",
                                         "containerPort":8080,
                                         "targetGroupArn":target_group_arn
                                        }
                                    ]),
    return response